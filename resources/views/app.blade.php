<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/css/app.css">

        <!-- Styles -->
        <style>
         .completed{
            text-decoration: line-through;
        }
        </style>
    </head>
    <body>
    </body>
    <main id="app">
        <template-header></template-header>
        <div class="py-4">
            <router-view></router-view>
        </div>
    </main>
    <script type="text/javascript" src="/js/app.js"></script>
    <script type="text/javascript"></script>
</html>
