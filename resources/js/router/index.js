import Home from '../views/Home'
import Todo from '../views/Todo'


export default {
	mode:"history",
	linkActiveClass:"active",
	routes:[
		{
			path:"/",
			name: "pages.home",
			component:Home
		},

		{
			path:"/todo",
			name: "pages.todo",
			component:Todo
		},
	],
}